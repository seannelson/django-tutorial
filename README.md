## Django Tutorial - Polls Application

This Django application is intended to follow the tutorial detailed [here](https://docs.djangoproject.com/en/3.2/intro/tutorial01/) to make a simple polls style web application that stores questions and allows users to vote on different choices for the questions. It is not intended to be complex, just to familiarise myself with the Django framework.
